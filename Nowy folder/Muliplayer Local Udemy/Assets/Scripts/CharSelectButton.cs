﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharSelectButton : MonoBehaviour
{
    public SpriteRenderer theSR;

    public Sprite buttonUP, buttonDown;

    public bool isPressed;

    public float waitToPopUp;
    private float popCounter;

    public AnimatorOverrideController theController;
   
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
        if (isPressed)
        {
         popCounter -= Time.deltaTime;

            if (popCounter <= 0)
            {
                isPressed = false;

                theSR.sprite = buttonUP;
            }
            
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && !isPressed)
        {

            PlayerController player = other.GetComponent<PlayerController>();

            if (player.theRb.velocity.y < -0.1f)
            {
                player.anim.runtimeAnimatorController = theController;


                isPressed = true;

                theSR.sprite = buttonDown;

                popCounter = waitToPopUp;
            }

            AudioManager.instance.PlaySFX(2);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player" && isPressed)
        {
            isPressed = false;

            theSR.sprite = buttonUP;

        }
    }

}
