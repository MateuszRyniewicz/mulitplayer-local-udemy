﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public GameObject playerSpawnEffect;

    public static GameManager instance;

    public string[] allLevels;
    private List<string> levelOrder = new List<string>();


    [HideInInspector]
    public int lastPlayerNumber;

    public int pointsToWin;
    private List <int> roundWins = new List<int>();

    private bool gameWon;

    public string winLevel;

    public void Awake()
    {

        if (instance == null)
        {

            instance = this;
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }

    }

    public int maxPlayer;

    public List<PlayerController> activePlayer = new List<PlayerController>();

    public bool canFight;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Keyboard.current.yKey.wasPressedThisFrame)
        {
            GoToNextArena();
        }
    }
    public void AddPlayer(PlayerController newPlayer)
    {
        if (activePlayer.Count < maxPlayer)
        {
            activePlayer.Add(newPlayer);

            Instantiate(playerSpawnEffect, newPlayer.transform.position, newPlayer.transform.rotation);
            
        }
        else
        {
            Destroy(newPlayer.gameObject);
        }
    }

    public void ActivatePlayers()
    {
        foreach(PlayerController player in activePlayer)
        {
            player.gameObject.SetActive(true);
            player.GetComponent<PlayerHealthController>().FillHealth();
        }
    }


    public int CheckActivePlayer()
    {
        int playerAliveCount = 0;

        for(int i = 0; i < activePlayer.Count; i++)
        {
            if (activePlayer[i].gameObject.activeInHierarchy)
            {
                playerAliveCount++;
                lastPlayerNumber = i;
            }
        }


        return playerAliveCount;
    }


    public void GoToNextArena()
    {
        //  SceneManager.LoadScene(allLevels[Random.Range(0, allLevels.Length)]);

        if (!gameWon)
        {



            if (levelOrder.Count == 0)
            {
                List<string> allLevelList = new List<string>();
                allLevelList.AddRange(allLevels);

                for (int i = 0; i < allLevels.Length; i++)
                {
                    int selected = Random.Range(0, allLevelList.Count);

                    levelOrder.Add(allLevelList[selected]);
                    allLevelList.RemoveAt(selected);
                }

            }

            string levelToload = levelOrder[0];
            levelOrder.RemoveAt(0);

            foreach (PlayerController player in activePlayer)
            {
                player.gameObject.SetActive(true);
                player.GetComponent<PlayerHealthController>().FillHealth();
            }

            SceneManager.LoadScene(levelToload);
        }
        else
        {
            foreach(PlayerController player in activePlayer)
            {
                player.gameObject.SetActive(false);
                player.GetComponent<PlayerHealthController>().FillHealth();
            }
            
            SceneManager.LoadScene(winLevel);
        }
    }

    public void StartFirstRound()
    {
        roundWins.Clear();
        foreach(PlayerController player in activePlayer)
        {
            roundWins.Add(0);
        }

        gameWon = false;

        GoToNextArena();
    }

    public void AddRoundWin()
    {
        if (CheckActivePlayer() == 1)
        {
            roundWins[lastPlayerNumber]++;

            if (roundWins[lastPlayerNumber] >= pointsToWin)
            {
                gameWon = true;
            }
        }
    }
}
