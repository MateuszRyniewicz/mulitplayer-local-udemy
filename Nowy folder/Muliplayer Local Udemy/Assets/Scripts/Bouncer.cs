﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : MonoBehaviour
{

    public SpriteRenderer theSr;

    public Sprite downSprite, upSprite;

    public float stayUpTime, bouncerPower;
    private float upCounter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (upCounter > 0)
        {
            upCounter -= Time.deltaTime;

            if (upCounter <= 0)
            {
                theSr.sprite = downSprite;
            }
        }   
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            upCounter = stayUpTime;
            theSr.sprite = upSprite;

            Rigidbody2D theRb = other.GetComponent<Rigidbody2D>();

            theRb.velocity= new Vector2(theRb.velocity.x, bouncerPower);

            AudioManager.instance.PlaySFX(6);
            
        }
    }
}
