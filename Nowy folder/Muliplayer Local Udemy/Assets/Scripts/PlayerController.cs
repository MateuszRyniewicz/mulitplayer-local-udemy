﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{

    public Rigidbody2D theRb;
    public float moveSpeed, jumpForce;

    private float velocity;

    public bool isGround;
    public Transform groundCheckPoint;
    public LayerMask whatIsGround;



    public Animator anim;

    public bool isKeyboard2;

    public float timeBetweenAttacks= 0.25f;
    private float attackCounter;

    [HideInInspector]
    public float powerUpCounter;
    private float speedStore, gravStore;

    // Start is called before the first frame update
    void Start()
    {

        DontDestroyOnLoad(gameObject);

        anim = GetComponent<Animator>();

        GameManager.instance.AddPlayer(this);

        speedStore = moveSpeed;
        gravStore = theRb.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {

        if (isKeyboard2)
        {
            velocity = 0f;

            if (Keyboard.current.lKey.isPressed)
            {
                velocity = 1f;
            }

            if (Keyboard.current.jKey.isPressed)
            {
                velocity = -1f;
            }

            if(isGround && Keyboard.current.rightShiftKey.wasPressedThisFrame)
            {
                theRb.velocity = new Vector2(theRb.velocity.x, jumpForce);
            }

            if(!isGround && Keyboard.current.rightShiftKey.wasReleasedThisFrame)
            {
                theRb.velocity = new Vector2(theRb.velocity.x, theRb.velocity.y * 0.5f);
            }

            if (Keyboard.current.enterKey.wasPressedThisFrame)
            {
                anim.SetTrigger("attack");

                attackCounter = timeBetweenAttacks;

                AudioManager.instance.PlaySFX(0);
            }
        }



        isGround = Physics2D.OverlapCircle(groundCheckPoint.position, 0.25f, whatIsGround);


        theRb.velocity = new Vector2(velocity * moveSpeed, theRb.velocity.y);

        /* 
         * if (Input.GetButtonDown("Jump"))
         {
             theRb.velocity = new Vector2(theRb.velocity.x, jumpForce);
         }

         */
        if (Time.timeScale != 0)
        {


            anim.SetBool("isGrounded", isGround);
            anim.SetFloat("ySpeed", theRb.velocity.y);
            anim.SetFloat("speed", Mathf.Abs(theRb.velocity.x));

            if (theRb.velocity.x < 0)
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }
            else if (theRb.velocity.x > 0)
            {
                transform.localScale = Vector3.one;
            }

        }


        if(attackCounter > 0)
        {
            attackCounter -= Time.deltaTime;

            theRb.velocity = new Vector2(0f, theRb.velocity.y);
        }

        if (powerUpCounter > 0)
        {
            powerUpCounter -= Time.deltaTime;

            if (powerUpCounter <= 0)
            {
                moveSpeed = speedStore;

                theRb.gravityScale = gravStore;
            }
        }

        
    }

    

    public void Move(InputAction.CallbackContext context)
    {
        
        velocity = context.ReadValue<Vector2>().x;
    }

    public void Jump(InputAction.CallbackContext context)
    {
        if (context.started && isGround)
        {
            theRb.velocity = new Vector2(theRb.velocity.x, jumpForce);

        }

        if(context.canceled && !isGround && theRb.velocity.y > 0f)
        {
            theRb.velocity = new Vector2(theRb.velocity.x, theRb.velocity.y * 0.5f);
        }
    }

    public void Attack(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            anim.SetTrigger("attack");

            attackCounter = timeBetweenAttacks;

            AudioManager.instance.PlaySFX(0);
        }
    }
    

    
}
