﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ArenaManager : MonoBehaviour
{
    public List<Transform> spawnPoints = new List<Transform>();

    private bool roundOver;

   

    public GameObject[] powerUps;
    public float timeBetweenPowerUps;
    private float powerUpsCounter;

    // Start is called before the first frame update
    void Start()
    {
      foreach(PlayerController player in GameManager.instance.activePlayer)
        {
            int randomPoint = Random.Range(0, spawnPoints.Count);
            player.transform.position = spawnPoints[randomPoint].position;

            if(GameManager.instance.activePlayer.Count <= spawnPoints.Count)
            {
                spawnPoints.RemoveAt(randomPoint);

            }

        } 
      

        GameManager.instance.canFight = true;
        GameManager.instance.ActivatePlayers();

        powerUpsCounter = timeBetweenPowerUps * Random.Range(0.75f,1.25f);
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.CheckActivePlayer() == 1 && !roundOver)
        {
            roundOver = true;

            //  GameManager.instance.GoToNextArena();
            StartCoroutine(EndRoundCo());
        }

        if (powerUpsCounter > 0)
        {
            powerUpsCounter -= Time.deltaTime;

            if (powerUpsCounter <= 0)
            {
                int randomPoint = Random.Range(0, spawnPoints.Count);
                Instantiate(powerUps[Random.Range(0, powerUps.Length)], spawnPoints[randomPoint].position, spawnPoints[randomPoint].rotation);

                powerUpsCounter = timeBetweenPowerUps * Random.Range(0.75f, 1.25f);
            }
        }
    }


    public IEnumerator EndRoundCo()
    {

        UiController.instance.winBar.gameObject.SetActive(true);
        UiController.instance.roundCompleteText.gameObject.SetActive(true);
        UiController.instance.playerWinText.gameObject.SetActive(true);
        UiController.instance.playerWinText.text = "Player:  " +(GameManager.instance.lastPlayerNumber + 1) + " wins!";

        GameManager.instance.AddRoundWin();

        yield return new WaitForSeconds(2f);


        UiController.instance.loadingScreen.SetActive(true);

        GameManager.instance.GoToNextArena();
    }
}
